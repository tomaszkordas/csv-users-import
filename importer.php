<?php

/**
Plugin Name: WP CSV/XML User Importer
Plugin URI: https://teachity.pl/wp/user-importer
Author: Teachity
Author URI: https://teachity.pl/
Version: 1.0
**/

require_once 'classes/class-importer.php';


if( ! defined('ABSPATH')){
	exit;
}


if(class_exists('Importer')){
	$importer = new Importer();
}
