<?php

class Users_Table extends WP_List_Table {

	var $data;

	var $found_data;

	function get_columns(){
		return array(
			'user_login' => '<strong>Login</strong>',
			'user_pass' => '<strong>Hasło</strong>',
			'role' => '<strong>Rola</strong>',
			'has_email' => '<strong>Posiada email</strong>',
			'generate_password' => '<strong>Wygenerowano hasło</strong>',
			'email_validated' => '<strong>Zwalidowano email</strong>',
			'created' => '<strong>Utworzono konto</strong>',
			'password_sent' => '<strong>Wysłano hasło</strong>',
			'error' => '<strong>Informacje</strong>',
		);
	}

	function prepare_items() {
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = array();
		$this->_column_headers = array($columns, $hidden, $sortable);
		$this->items = $this->data;

		$per_page = 80;
		$current_page = $this->get_pagenum();
		$total_items = count($this->data);

		// only ncessary because we have sample data
		$this->found_data = array_slice($this->data,(($current_page-1)*$per_page),$per_page);

		$this->set_pagination_args( array(
			'total_items' => $total_items,                  //WE have to calculate the total number of items
			'per_page'    => $per_page                     //WE have to determine how many items to show on a page
		) );
		$this->items = $this->found_data;

	}

	function column_default( $item, $column_name ) {

		switch( $column_name ) {
			case 'error':
			case 'user_login':
			case 'user_pass':
				return $item[ $column_name ];
			case 'role': return !$item[ $column_name ] || $item[ $column_name ] == "" ? "Brak" : $item[ $column_name ]; break;
			case 'has_email':
			case 'generate_password':
			case 'email_validated':
			case 'password_sent':
			case 'created':
				if($item[ $column_name ] === false) return "&#x2716;"; // &#2714;
				if($item[ $column_name ] === true) return "&#x2714;"; // &#2716;
				break;
			default:
				return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
		}
	}

}

$file_d = get_option('importer_file_path');

$site_title = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );

$content = $file_d ? Importer::getLines($file_d, ";", 1) : false;

$content_head = false;
if(is_array($content) && count($content)){
	$content_head = $content[0];
}

$roles = wp_roles();

$serialized_results = get_option('imported_users');

if($serialized_results){
	$results = unserialize($serialized_results);

	if($_GET['action'] == 'delete'){

	    foreach ($results as $user){

	        $user = get_user_by('login', $user['user_login']);
	        if($user) {
				wp_delete_user($user->ID);
			}

        }

		delete_option('imported_users');

		$results = null;

    }

}


if(isset($_POST['but_import'])) {

	$login_index = isset($_POST['login']) && $_POST['login'] != "" ? intval($_POST['login']) : false;
	$password_index = isset($_POST['password']) && $_POST['password'] != "" ? intval($_POST['password']) : false;
	$email_index = isset($_POST['email']) && $_POST['email'] != "" ? intval($_POST['email']) : false;
	$nicename_index = isset($_POST['nicename']) && $_POST['nicename'] != "" ? intval($_POST['nicename']) : false;
	$display_name_index = isset($_POST['display_name']) && $_POST['display_name'] != "" ? intval($_POST['display_name']) : false;
	$role = isset($_POST['role']) && $_POST['role'] != "" ? trim(strip_tags($_POST['role'])) : false;

	$roles = wp_roles();
	$role = in_array($role, array_keys($roles->role_names)) ? $role : "";

	$user_rows = Importer::getLines($file_d, ";");

	$results = array();



	foreach ($user_rows as $row_index => $data){
		if($row_index > 0){

			$result = array();

			$user = new WP_User();
			$user->user_login = is_integer($login_index) ? $data[$login_index] : null;
			$user->user_pass = is_integer($password_index) ? $data[$password_index] : null;
			$user->user_email = is_integer($email_index) ? $data[$email_index] : null;
			$user->user_nicename = is_integer($nicename_index) ? $data[$nicename_index] : null;
			$user->display_name = is_integer($display_name_index) ? $data[$display_name_index] : null;

			$result['user_login'] = $user->user_login;
			$result['user_pass'] = $user->user_pass;
			$result['role'] = $role;

			if($user->user_email !== null) {
				$result['has_email'] = true;
				if (filter_var($user->user_email, FILTER_VALIDATE_EMAIL)) {
					$user->user_pass = wp_generate_password(20, false);
					$result['user_pass'] = "&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;";
					$result['generate_password'] = true;
					$result['has_email'] = true;
					$result['email_validated'] = true;
				} else {
					$user->user_email = null;
					$result['user_email'] = $user->user_email;
					$result['generate_password'] = false;
					$result['has_email'] = false;
					$result['email_validated'] = false;
				}
			} else {
				$result['generate_password'] = false;
				$result['has_email'] = false;
				$result['email_validated'] = false;
			}

			$user_id = wp_insert_user($user);

			if($user_id instanceof WP_Error){
				$result['role'] = null;
				$result['password_sent'] = false;
				$result['generate_password'] = false;
				$result['created'] = false;
				$result['error'] = $user_id->errors['existing_user_login'][0];

			} else {

				wp_update_user(array('ID' => $user_id, 'role' => $role));
				$result['password_sent'] = false;
				$result['created'] = true;
				$result['error'] = null;

				if($result['has_email'] && $result['email_validated']){

					$subject = "[".$site_title."] Login data";

					$email_message = "Username: ".$user->user_login."\r\n\r\n";

					$email_message .= "Password: ".$user->user_pass."\r\n\r\n";

					$email_message .= wp_login_url() . "\r\n";

					wp_mail($user->user_email, $subject, $email_message);

					$result['password_sent'] = true;
				}
			}

			$results[] = $result;

		}
	}

	$serialized_results = serialize($results);

	add_option('imported_users', $serialized_results);

	$userList = new Users_Table();
	$userList->data = $results;
	$userList->prepare_items();



} else {

	if (isset($results)) {

		$userList = new Users_Table();
		$userList->data = $results;
		$userList->prepare_items();

	}
}



?>

<?php if(isset($userList)) : ?>
<h2>Podsumowanie</h2>
<a href="?page=importer&action=delete" class="button button-delete">Usuń poniższych użytkowników</a>
<?php $userList->display(); ?>
<?php else : ?>
<h2>Ustawienia parametrów nowych użytkowników</h2>
<form method="post" action="" name="convertcsv" enctype='multipart/form-data'>
	<table class="form-table" role="presentation">
		<tr>
			<th>Login</th>
			<td><select name="login" <?php echo $file_d ?: "disabled" ?>>
					<option value><?php _e( '&mdash; Empty &mdash;' ); ?></option>
					<?php foreach ($content_head as $index => $item) : ?>
					<option value="<?php echo $index ?>"><?php echo $item ?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<th>Hasło</th>
			<td><select name="password" <?php echo $file_d ?: "disabled" ?>>
					<option value><?php _e( '&mdash; Empty &mdash;' ); ?></option>
					<?php foreach ($content_head as $index => $item) : ?>
						<option value="<?php echo $index ?>"><?php echo $item ?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<th>Email</th>
			<td><select name="email" <?php echo $file_d ?: "disabled" ?>>
					<option value><?php _e( '&mdash; Empty &mdash;' ); ?></option>
					<?php foreach ($content_head as $index => $item) : ?>
						<option value="<?php echo $index ?>"><?php echo $item ?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<th>Nick</th>
			<td><select name="nicename" <?php echo $file_d ?: "disabled" ?>>
					<option value><?php _e( '&mdash; Empty &mdash;' ); ?></option>
					<?php foreach ($content_head as $index => $item) : ?>
						<option value="<?php echo $index ?>"><?php echo $item ?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<th>Wyświetlana nazwa</th>
			<td><select name="display_name" <?php echo $file_d ?: "disabled" ?>>
					<option value><?php _e( '&mdash; Empty &mdash;' ); ?></option>
					<?php foreach ($content_head as $index => $item) : ?>
						<option value="<?php echo $index ?>"><?php echo $item ?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<th>Rola</th>
			<td><select name="role" <?php echo $file_d ?: "disabled" ?>>
					<option value><?php _e( '&mdash; No role for this site &mdash;' ); ?></option>
					<?php foreach ($roles->roles as $index => $item) : ?>
						<option value="<?php echo $index ?>"><?php echo $item['name'] ?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<th><input type='submit' name='but_import' class="button button-primary <?php echo $file_d ?: "button-disabled" ?>" value="Utwórz użytkowników" <?php echo $file_d ?: "disabled" ?>></th>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
<?php endif; ?>