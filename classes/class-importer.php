<?php

class Importer
{

    public function __construct()
	{

		register_activation_hook(__FILE__, array($this, 'activate'));

		register_deactivation_hook(__FILE__, array($this, 'deactivate'));

		add_action( 'admin_menu', array($this, 'add_admin_panel_pages') );

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );
	}

	public function activate(){
		flush_rewrite_rules();
    }


	public function deactivate(){
		flush_rewrite_rules();
	}

	public function enqueue(){

		wp_enqueue_style('importer-style', plugins_url().'/importer/css/style.css', false);

		wp_enqueue_script('importer-script', plugins_url().'/importer/js/main.js', 'jquery', false, true);

	}

	public function settingsForm(){

	?>

		<div class="wrap">
            <h1><i>WP CSV/XML User Importer</i></h1>
			<?php

            global $error;
            $error = new WP_Error();

            include_once plugin_dir_path(__FILE__)."/uploadfile.php";
			include_once plugin_dir_path(__FILE__)."/createuser.php";

			?>
		</div>


	<?php

	}

	public static function getLines($path, $divider, $length = null){

		$content = fopen($path, 'r');

		$array = array();

		$line_nr = 0;

		while (($line = fgetcsv($content, 1000, $divider)) !== false){

		    $array[] = $line;

		    ++$line_nr;

		    if(is_integer($length) && $line_nr >= $length) {
		        return $array;
            }

		}

		return $array;

	}


	public function add_admin_panel_pages(){

		add_menu_page('Importer', 'CSV/XML User Importer', 'manage_options', 'importer', array($this, 'settingsForm'), 'dashicons-businessperson'/*, plugins_url( 'importer/icon.png' )*/);

	}

}