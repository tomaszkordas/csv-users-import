<?php

$file_d = get_option('importer_file_path');
$file_url = get_option('importer_file_url');

if(!file_exists($file_d)){

	$file_d = false;

	delete_option('importer_file_path');
	delete_option('importer_file_url');
	delete_option('imported_users');
}


function get_nopaging_url() {

	return plugins_url().'/importer/class/uploadfile.php';

}

function get_file_name( $path ){

    $path_arr = explode('/', $path);

    return end($path_arr);

}

if(isset($_POST['but_submit'])){

	if($_FILES['file']['name'] != ''){

		$uploadedfile = $_FILES['file'];

		$csv_mimetypes = array(
			'text/csv',
			'text/plain',
			'application/csv',
			'text/comma-separated-values',
			'application/excel',
			'application/vnd.ms-excel',
			'application/vnd.msexcel',
			'text/anytext',
			'application/octet-stream',
			'application/txt',
            'application/xml',
            'text/xml'
		);

		if(!$uploadedfile || !in_array($uploadedfile['type'], $csv_mimetypes)){

			$error->add(409, 'Nieobsługiwany typ pliku.');

        } else {

			$upload_overrides = array('test_form' => false);

			$movefile = wp_handle_upload($uploadedfile, $upload_overrides);
			$imageurl = "";
			if ($movefile && !isset($movefile['error'])) {

				if ($file_d === false) {
					add_option('importer_file_path', $movefile['file']);
					add_option('importer_file_url', $movefile['url']);
				} else {
					update_option('importer_file_path', $movefile['file']);
					update_option('importer_file_url', $movefile['url']);
				}

				$file_d = $movefile['file'];
				$file_url = $movefile['url'];

			} else {
				$error->add(409, 'Przepraszamy, ten typ pliku jest niedozwolony ze względów bezpieczeństwa.');
			}
		}
	}

}

if(isset($_POST['but_delete'])){

    $path = explode('/',$file_d);
    $start = false;
    $subpath = '/';
    foreach ($path as $index => $value){
        if($start){
            $subpath .= $value.'/';
        }
		if($value == 'uploads'){
			$start = true;
		}
    }

    $file_path = rtrim(wp_get_upload_dir()['basedir'].$subpath, "/");

    unlink($file_path);
    $file_d = false;
    delete_option('importer_file_path');
    delete_option('importer_file_url');
    delete_option('imported_users');

}




if(is_wp_error($error) && count($error->get_error_messages())) {  ?> <div class="error notice"><p><?php echo $error->get_error_message(); ?> </p></div> <?php }


?>


    <h2>Prześlij plik *.csv z danymi użytkowników.</h2>
    <h4 style="color:red">Przesłany plik będzie publiczny, pamiętaj o usunięciu go po zakończeniu procesu importu.</h4>

<?php if(!$file_d) {

    ?>

    <form method="post" action="" name="importer_upload_file" enctype='multipart/form-data'>
        <table class="form-table" role="presentation">
            <tr>
                <th>Prześlij plik</th>
                <td><input type='file' name='file'></td>
            </tr>
            <tr>
                <th>
					<input type='submit' name='but_submit' class="button button-primary" value='Prześlij'>
				</th>
				<td></td>
            </tr>
        </table>
    </form>

<?php } else { ?>

    <form method="post" action="" name="importer_upload_file" enctype='multipart/form-data'>
        <table class="form-table" role="presentation">
            <tr>
                <th>Przesłany plik *.csv</th>
                <td><a href="<?php echo $file_url; ?>" target="_blank"><?php echo get_file_name($file_d); ?></a></td>
            </tr>
            <tr>
                <th><input type='submit' id='but_delete' name='but_delete' class="button button-cancel" value="Usuń plik z serwera"></th>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>

<?php } ?>