<?php

$file_d = get_option('importer_file_path');

if(isset($_POST['but_convert'])) {

	$divider = $_POST['separator'];
	if($divider == 'comma') $divider = ",";
	elseif ($divider == 'semicolon') $divider = ";";
	elseif ($divider == 'tab') $divider = "??";
	elseif ($divider == 'space') $divider = "??";
	else $divider = ";";


	$content = $file_d ? Importer::getLines($file_d, $divider, 10) : false;

	$content_head = isset($_POST['first_is_header']) ? true : false;
	if ($content_head && is_array($content) && count($content)) {
		$content_head = $content[0];
	}

	class List_Table extends WP_List_Table {

		function __construct($args = array())
		{
			parent::__construct($args);

		}

		var $rows = array();

		var $columns = [];

		function get_columns(){
			return $this->columns;
		}

		function prepare_items() {
			$columns = $this->get_columns();
			$hidden = array();
			$sortable = array();
			$this->_column_headers = array($columns, $hidden, $sortable);
			$this->items = $this->rows;;
		}

		function column_default( $item, $column_name ) {

			switch( $column_name ) {
				case 0:
				case 1:
				case 'isbn':
					return $item[ $column_name ];
				default:
					return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
			}
		}

	}



	$myListTable = new List_Table();
	$myListTable->rows = $content;
	$myListTable->columns = $content_head;
	$myListTable->prepare_items();
	$myListTable->display();

}

?>

<form method="post" action="" name="convertcsv" enctype='multipart/form-data'>
	<table class="form-table" role="presentation">
		<tr>
			<th>Divider</th>
			<td><select name="separator">
					<option value="comma">,</option>
					<option value="semicolon">;</option>
					<option value="tab">{tab}</option>
					<option value="space">{space}</option>
				</select>
			</td>
		</tr>
		<tr>
			<th>First row is header</th>
			<td><input type="checkbox" name="first_is_header" value="true" checked>
			</td>
		</tr>
		<tr>
			<th><input type='submit' name='but_convert' class="button button-primary" value="Convert data"></th>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>

<?php if(isset($content)) : ?>

<table class="widefat fixed">
	<?php if(isset($content_head)) : ?>
	<thead>
		<tr>
			<?php foreach ($content_head as $item) : ?>
			<th><?php echo $item; ?></th>
			<?php endforeach; ?>
		</tr>
	</thead>
	<?php endif; ?>
</table>

<?php endif; ?>
